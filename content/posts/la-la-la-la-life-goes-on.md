+++
author = "David Rhoads"
date = 2020-09-24T01:25:00Z
description = "How things got busy"
image = "/uploads/aaron-blanco-tejedor-vbe9zj-jhbs-unsplash.jpg"
image_webp = "/uploads/aaron-blanco-tejedor-vbe9zj-jhbs-unsplash.webp"
title = "La la la la life goes on"

+++
Even in this crazy time, things were bound to get busier. Entering the semester, I somehow had the idea that the coronavirus pandemic would make things a bit easier at school. Sure, we would still have to do homework and show up for class, but perhaps a wave of generosity would sweep through campus, endowing each professor with the gift of patience and leniency... Unfortunately I was wrong.

Don't get me wrong - every one of my professors this semester is a decent human being with nothing but the best intentions for any of their students. Perhaps they realize that going soft on their students would only contribute to the loss of education sustained near the end of last semester, where classes moved online with virtually no transition period. So I don't harbor a grudge against my professors. My grudge is largely aimed at myself for impulse-registering for that extra class. Sure, I could drop it, but...self-esteem.

I have every intention of passing all my classes this semester. I just sometimes lose faith that it will actually happen. More than the actual work is the time commitment. I have limited hours in the day, so I am forced to make decisions about where to put my time. Is it really worth watching that hour long calc video, or should I be doing my accounting homework instead? When is it time to leave it all and have some outdoor time?

The most important thing I need to do is to constantly remind myself that I am only human. There is only so much I am capable of doing. It's important to push myself academically, but if I push too hard, I'll burnout, and then I'll be worse off than before. I need to remind myself every day that school is only school. This too shall pass.

{{<comments la-la-la-la>}}