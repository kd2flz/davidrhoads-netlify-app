+++
author = "David Rhoads"
date = 2021-01-02T02:50:00Z
description = "Check out the newly-named Google Docs killer!"
image = "/uploads/piotr-laskawski-gxmr7badxqo-unsplash.jpg"
title = "Coming To You Live (From Hedgedoc)"

+++
First of all, it's been a very long time since I wrote a blog. Life has kind of got in the way for a while. Nothing bad though. Most college students spend their Christmas break relaxing and playing video games. I, however, somehow contrived to line up 2 regular volunteer opportunities *and* winter classes, so I've had to navigate that. That said, I think things are finally falling into place, and I'm in a good position to do well in my classes, and still have some time left over to chill. 

Now to the title(and main point) of this blog. I've been a regular listener on both the Ask Noah Show and Linux Unplugged for years. Both shows have mentioned "CodiMD" countless times. I knew what it was (an interactive markdown editor), but wasn't too sure what that was or why I would need that. However, the projects recent \[renaiming\](([https://community.hedgedoc.org/t/codimd-becomes-hedgedoc/170](https://community.hedgedoc.org/t/codimd-becomes-hedgedoc/170 "https://community.hedgedoc.org/t/codimd-becomes-hedgedoc/170"))) created a minor media buzz which rekindled my own personal interest in this project.

Enter the all new \[Hedgedoc\]([https://hedgedoc.org/](https://hedgedoc.org/ "https://hedgedoc.org/")). Hedgedoc is an interactive markdown editor (I know I said that already). However, a better way to describe Hedgedoc (as Chris and Alex did on the latest episode of \[Self Hosted\]([https://selfhosted.show/36](https://selfhosted.show/36 "https://selfhosted.show/36"))), is a "Google Docs Replacement". I mean that seriously. If you use Google Docs as a full on Desktop Publishing suite, this may not be the case (but check out \[OnlyOffice\]([https://www.onlyoffice.com/](https://www.onlyoffice.com/ "https://www.onlyoffice.com/"))). However, that is totally not my use case with Google Docs. I am quite happy with LibreOffice, and I have been known to use MS Office on school computers if I have to. 

I have mostly used Google Docs as a place to house collaborative documents, particularly for Group Projects I'm working on at school. Rarely have I ever not needed to reformat the contents of this collaborative document into a Writer or Word doc. For this sort of collaborative editing, Hedgedoc is perfect. The collaboration is honestly mind blowing. I have my instance running on a VPS with 1 VCPU and 2GB of memory. I imagine this would be completely insufficient were I running any kind of production instance. However, for a personal instance where I occasionally have several people on simultaneously it should work fine. For reference, as I edit this document I'm at about 30% of my memory and up to 5% of my CPU. 

My only hesitation about using this amazing app is I'm not sure how other students will handle the Markdown aspect. On one hand, it is super straightforward - and Hedgedoc even offers clicky buttons for bold, italics, links, etc. However, it does have that hacker vibe to it, which I adore, but could scare some totally non hacker type students in my major. I guess I'll just have to try in and find out.

Oh also, it has a dark mode (that deserved its own paragraph).

Finally I get to setup. For me it was pretty straightforard. I used the linuxserver docer image. Hedgedoc has their own image - perhaps I'll try that at some point in the future. Their documentation seems to be pretty good, which is always nice.

To be honest, the only problems I had getting started are 

1\. I don't really know json, so I had to figure that out.

2\. I had problems figuring out how to register for an account :smile: Turns out you have to fill in your username and password in the sign up and click register (instead of sign in). I kept clicking register and expecting to get sent to a sign-up page!

So that about does it with my review of Hedgedoc. It seems to be a solid project, and I'm already a big fan. I'll see what other cool use cases I come up with for it. If I find any, I'll write about them right here on davidrhoads.me.

{{<comments coming-to-you-live>}}