+++
author = "David Rhoads"
date = 2021-05-04T14:00:00Z
description = "Life and liberty are fairly concrete, but happiness?"
image = "/uploads/marek-piwnicki-e7ZWXOuxjZY-unsplash.jpg"
title = "What Happiness Is"
+++

A few hundred years ago, some dudes with wigs sat down and wrote the US Declaration of Independence. Although this document had political implications (the beginning of a new country, and the beginning of the Revolutionary War), it was also a philosophical document, arguing that people are endowed by God with "certain unalienable rights" such as "life, liberty, and the pursuit of happiness." Government, the founders argued, exists to protect these rights. While this is a radical claim (and it still is today, in many parts of the world) that is the topic for another discussion. 

Personally, I've always been fascinated by the inclusion of happiness among the rights listed in the Declaration. The "natural rights" put forward some time earlier by John Locke included Life, Liberty, and Property. Property is substantially less squishy than happiness, so why did the founders make the substitution? It's possible that they took inspiration from Locke's natural rights, but realized the impossibility of sustaining a state that lacked the power of taxation. If happiness wasn't property, it could still be caused by property, meaning the founders weren't completely disagreeing with Locke.

Happiness is a paradox. When someone prioritizes their own happiness over the happiness of others in a society, this can result in a decrease in societal happiness. In addition, from my own experience, people who prioritize their own happiness are not as happy as people who put other peoples' happiness in front of their own. Put simply, generous people are often happier than selfish people. These beneficent people achieve happiness, even though they are not pursuing it. 

Happiness is intertwined with people. People are created to be together. Some people achieve happiness through romantic love. Others through caring for children or the elderly. Still others through social groups such as book clubs or sports teams. Even gamers, who are often characterized as being "loners", are frequently intertwined in massive online "communities". An avatar on a screen is no substitute for a real person, but it's a heck of a lot better than no one at all. While pleasure can be achieved on one's own, true happiness requires other people. 

What really causes happiness? Why did we evolve to desire it? Doubtless, their are a plethora of academic papers on the subject. However, my message is simple. If you are unhappy, put your phone on "Do Not Disturb" and go spend an hour with a friend.

{{<comments what-happiness-is>}}