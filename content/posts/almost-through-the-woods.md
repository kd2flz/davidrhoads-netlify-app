+++
author = "David Rhoads"
date = 2021-04-25T12:00:00Z
description = "Sixteen Years of Schooling, Almost Done."
image = "/uploads/kimberly-farmer-lUaaKCUANVI-unsplash.jpg"
title = "Almost Through The Woods"

+++

The woods in this case is my sixteen years of primary and higher education. That's right. Since September 2004, I have been dedicating myself to filling my mind with knowledge. What's that you say? 2021 - 2004 = 17? Oh yeah that's right, I *did* have a gap year in between high school and college. Thank goodness. I don't know that I would have survived college without it.

Four years later, I'm about to graduate Siena College with a bachelor's degree in Accounting. I'm ready to move on to the next chapter in my life. School has been good to me. Honestly, I wouldn't be writing this blog to post on a Hugo-powered static site of my own creation had it not been for the web design classes I took over the last four years. Nor would I be typing it in the terminal on my beloved Linux-based PopOS had it not been for several classes which sparked my interest in Linux. Hundreds of hours of podcasts and YouTube videos later, It's the operating system I use all day every day (except when I fire up my underpowered Windows machine to run lockdown browser for exams :( -  That is one thing I will never miss from school). 

This last year has been a tough year to attend university. Across the country, students have dropped out, postponed enrollment, or given up on ever even attempting a higher education. While anyone privileged enough to attend college in the US is already doing better than many around the world, a year of soul killing zoom classes is enough to dishearten even the most cheerful person. While those forced to work remotely at least retain an income stream, many students have been forced to pay equivilent tuition for an inferior education. My school, thank goodness, has at least tried to reopen in person, but the majority of my classes remain online, to the point where it wasn't worth commuting for the two in-person classes, only to be forced to attend the rest of my zoom classes from the library (or some similarly unequipped location).

So all that is coming to a close, and I don't regret it. What I will regret is having the excuse to learn things. I fully intend to be a lifetime learner. But there is something to be said for deadlines, required readings, and research assignments. As much as my current self loathes the pressure, it's undeniable that these things have made me a better and more knowledgeable person.

An aside: for the record, this is my first blog post composed and published completely from the terminal. Hopefully it works, as I'm kind of digging the experience.

{{<comments almost-through-the-woods>}}
