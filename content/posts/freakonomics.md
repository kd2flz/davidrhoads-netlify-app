+++
author = "David Rhoads"
date = 2021-12-22T18:23:00Z
description = "Book Review of Freakonomics by Steven Levvit and Steven Dubner."
title = "Freakonomics"
category="Book Review"

+++

{{<rawhtml>}}
<a href="https://www.goodreads.com/book/show/1202.Freakonomics" style="float: left; padding-right: 20px"><img border="0" alt="Freakonomics: A Rogue Economist Explores the Hidden Side of Everything" src="https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1550917827l/1202._SX98_.jpg" /></a><a href="https://www.goodreads.com/book/show/1202.Freakonomics">Freakonomics: A Rogue Economist Explores the Hidden Side of Everything</a> by <a href="https://www.goodreads.com/author/show/798.Steven_D_Levitt">Steven D. Levitt</a><br/>
My rating: <a href="https://www.goodreads.com/review/show/3768558653">4 of 5 stars</a><br /><br />
Freakonomics is a classic - I've been meaning to read it for years. When I finally picked it up, I wasn't disappointed. I've been an avid listener of Dubner's "Freakonomics Radio" for years, and this book reads like a number of extended episodes. Rather than try to use current events or everyday concepts to explain economics, Levvit and Dubner use economics to explain these common concepts. <br />It's easy to misconstrue some of the book's arguments. Therefor, it's important to view the book as a commentary rather than a policy playbook. <br /><br />When Levvit and Dubner argue that legalizing abortion decreased the crime rate, they are not arguing for increasing abortion rates, or for that matter even legal abortion; they are simply stating that if economic analysis is applied to the historical data, this is the conclusion they reach. For that matter, their argument could just as easily be used by opponents of abortion - the reason legalized abortion purportedly decreased crime is that it wipes out a segment of society that are socially or economically predisposed to be criminals. If the reader views Levvit and Dubner as unbiased observers rather than policy wonks, many of the criticisms of this book are rendered ineffectual.<br /><br />While this book has aged two decades, the method of thinking it promotes is even more relevant than it was when the book was originally published.
<br/><br/>
<a href="https://www.goodreads.com/review/list/69126587-david">View all my reviews</a>
{{<rawhtml>}}