+++
author = "David Rhoads"
date = 2021-08-28T14:00:00Z
description = "It's worth the risk"
image = "/uploads/stephen-walker-k86HbvIIsmg-unsplash.jpg"
title = "Smile."

+++
> What do I do when my love is away?  
> Does it worry you to be alone?  
> How do I feel by the end of the day?  
> Are you sad because you're on your own?  
> No, I get by with a little help from my friends  
> Mm, I get high with a little help from my friends  
> Mm, gonna try with a little help from my friends  
 -- The Beatles  


Last week, I joined some friends for lunch. Over a home-cooked meal, we discussed work, family, and other such trivialities. The conversation was enjoyable, the food delicious, and the experience was altogether a good one. Soon after lunch, however, a coworker confronted me over a misunderstanding. The ensuing interaction completely derailed my afternoon. Although he and I later resolved our differences, this incident reminded me of the mood-altering power of the smile.

A smile is not universally good. Smiles sometimes come at the expense of other people. Life can be hard, and smiles are not always easy to come by. Forcing a smile to cover up pain or grief can be a terrible mistake. My point here is not to hold up the smile as being a symbol of goodness (although it can be that). Rather, I would argue for setting a smile as your default facial expression.

The evening of the incident with my coworker, I sat on the edge of my bed and recalled my day. I put the video tape of my life into my mental VCR and pressed rewind. When I reached 1 PM I pressed play, and watched again the ill-fated conversation. Next I imagined if both I and the coworker had been smiling. This imagined conversation was much shorter, and ended in laughter and a handshake. 

Dare to smile. Sometimes people will misunderstand your smile. Take the risk. A misunderstood smile is better than an understood frown. 


{{<comments smile>}}