+++
author = "David Rhoads"
date = 2020-09-05T01:00:00Z
description = "Returning to school in the time of COVID-19"
image = "/uploads/dog-734689_1920.jpg"
image_webp = "/uploads/dog-734689_1920.webp"
title = "Back to school"

+++
## It happened.

I'm back at school. When I arrived back at my apartment last week, I paused to reflect on the nearly six months since I had left school for spring break.

I attend Siena College, which is located in Loudenville, New York. It is just outside of Albany - the state capital. When I left campus for the last time in March, upstate New York was still in the throes of winter. Now I've returned to Albany, just in time to see a week or two of summer. I expect the leaves to start changing in a few weeks time. In the time since I left campus, my state has suffered through one of the worst outbreaks of COVID-19 in the world. Returning to school, everything is so _normal_, but so different, it's almost eerie.

Siena's administration has been working overtime this summer to ensure we have a decent semester. By testing all students before school, banning visitors and parties, mandating masks, and making use of previously unused spaces they believe they can create a relatively safe environment for students and faculty. Although I knew all this was happening, I did a double take when I saw the large white "classroom tents" on the quad.

Added to this is the fact that most classes have split their time between online and in-person classes. This is partly because of a limit on the number of students allowed in the classroom, and partly to minimize general student traffic in the academic buildings. One class I have, for instance, splits the class in half, with one half coming in and the other half on Zoom. The next day, the groups will swap places.

I'm slowly sliding into this new rhythm. Take the 182 bus to school. Go to class. Take the bus home. Join a few Zoom meetings. Do some homework. Repeat. Part of me actually enjoys having more time at home. I much prefer studying in my room to the school's library. However, there's the lingering fear of another lockdown. It could happen so fast. Before we returned to school, we had to submit an "in case of emergency" plan, giving detailed instructions about where we would go if the school had to close. Already this week, SUNY Oneonta, 1 1/2 hours drive south-west, was closed after an outbreak among students.

This school year will be one for the history books. I often wish it was there already, but I'm also quite curious to see how it plays out.

{{<comments back-to-school>}}
