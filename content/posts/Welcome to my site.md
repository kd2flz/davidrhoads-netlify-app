---
title: "Welcome to my site!"
date: 2020-08-29
image_webp: images/blog/welcome.webp
image: images/blog/welcome.jpg
author: David Rhoads
description : "Welcome to davidrhoads.me!"
---

This site has been in the works for a long time. I’ve been messing with Hugo for most of the summer. It took a while to get a good workflow set up, but now, using Hugo, Github, and Netlify, I believe I have a sustainable workflow.

> Thank you to everyone who gave me inspiration.

I want to thank those who helped me get to this point (mostly unknowingly). Special thanks go to Brian Lunduke and Chris Titus Tech for their videos on Hugo. 

This site is a place for me to chronicle my journey through life. I will write about tech, but also about my thoughts on life. 

Finally, I am a lifetime learner. I am fairly new to web design, and even newer to blogging. As such, I welcome any feedback. Send me an email at davidrhoads@davidrhoads.me. <div>You can find also find me on <a rel="me" href="https://fosstodon.org/@daver98">Mastodon</a>.</div>

{{<comments welcome-to-my-site>}}