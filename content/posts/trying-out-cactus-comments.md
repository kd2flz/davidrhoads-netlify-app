+++

author = "David Rhoads"
date = 2021-03-01T18:00:00Z
description = "All-new Matrix powered embeddable comments system."
image = "/uploads/scott-webb-GQD3Av_9A88-unsplash.jpg"
title = "Trying out Cactus Comments"

+++
I tend to ignore website comments. They often turn into a den of iniquity. However, a new project, [Cactus Comments](https://cactus.chat), rethinks website comments by integrating them into an existing powerful chat network. This network is Matrix.

Matrix offers a decentralized chat ecosystem. While you can visit [https://app.element.io](https://app.element.io) and create a free account, you can also host your own instance, or sign up for an account on an independent server (such as [element.linuxdelta.com](https://element.linuxdelta.com)). Element, the interface designed by the matrix.org team, is the most widely used interface for Matrix. However, because Matrix is open-source, many developers have built their own interface to the network. Cactus Comments is one of these.

So what is Cactus Comments? In short, it is an embeddable Matrix client combined with a self-hostable server and bot. To get started, follow [these](https://cactus.chat/docs/getting-started/introduction/) easy instructions by the Cactus Comments developers. 

I was honestly impressed by how easy it was to get started with Cactus Comments. While it still has some rough edges (profile pictures it pulls in from the Matrix network are slightly blurry), the project seems to be under active development, so these will likely be ironed out in the near future. 

My best part about the project is that it integrates with static sites (such as this one). While there are probably a few ways to accomplish this, I created a shortcode in my Hugo site which contains the code for the comments section. This shortcode accepts a variable for the chat ID, which must be different for every comment section. Essentially, the bot creates a new room for each comment section. These rooms can then be accessed either from the web page, or from another Matrix client like Element. In fact, Cactus Comments provides a handy matrix.to link to view the room in your preferred client. 

Finally, it is relatively easy to moderate comments. The Cactus Comments bot invites you to a "moderation" room for each site you create. This Matrix room allows you to view, reply to, and delete comments for all the pages across your site. If you have the Element client on your phone, you should now get a notification for every comment on your website. (Of course, you can tailor notifications to your needs inside the client). 

{{<comments trying-out-cactus-comments>}}