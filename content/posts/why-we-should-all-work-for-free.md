+++
author = "David Rhoads"
date = 2020-10-12T17:00:00Z
description = "Why working for money may not be a good idea"
image = "/uploads/jp-valery-hfrdzaxwb5c-unsplash.jpg"
image_webp = "/uploads/jp-valery-hfrdzaxwb5c-unsplash.jpg"
title = "Why we should all work for free"

+++
Image Credit: Photo by Jp Valery on Unsplash

I work a job. Sure it's a part-time job (I'm a full-time student), but I get paid for my work. However... At risk of sounding like a hypocrite, here's why I think working for money may not be a good idea.

Last night, I watched a very outdated documentary about hacking. It was a requirement for my class. I would not recommend watching this documentary, mostly because much of the information in it seems quite outdated, and could be misleading if taken as fact in 2020. However, there were one point that echoed with me. One of the people who was interviewed for the video said that he believed "grey hat" hackers were more effective than "white hat" hackers who were on a salary from the company or paid consultants. He argued this is because doing something for money takes the fun out of it. If you're working a 9-5 job as a security professional, you're likely very good at what you do. However, you probably are motivated by your paycheck, rather than the natural curiosity that motivates so many "hobby"  hackers.

Another area where compensation decreases work quality is open source software development. A good illustration of this is the recent controversy surround Digital Ocean's Hacktoberfest. For an in depth look at what happened, see [this article](https://joel.net/how-one-guy-ruined-hacktoberfest2020-drama?guid=none&deviceId=277f7e9c-4043-41a2-8aa4-9ac5fb0ab6de) by Joel Thoms. The short version of the story is that Digital Ocean's annual Hacktoberfest contest, which aims to teach people how to contribute to open source projects, ended up essentially spamming projects with junk pull requests. While there are many reasons for this (again, read the article I linked above), the underlying problem was that Digital Ocean provided compensation to these people (albeit in the form of a free T-Shirt) in exchange for their contributions. This clearly achieved it's stated goal of increasing the number of contributions. However, instead of each contribution being a well thought through change, suggestion, or bug fix, many of them were of little to no use.

So should we all work for free? Ideally, I think we should. Right now, this is not really possible. However, I think it's worth considering this idea. What if you didn't need to make money on your work? This could happen either through a system of Universal Basic Income, or even more radically, a society that lived without money. Even if it seems unlikely that such systems could work on a national level, it's easy to see how a localized version of either could get off the ground.

In today's reality, though, software developers have to eat, so if you are able, make a donation to an open source project that you use on the regular. And be sure to dream big. 

{{<comments why-we-should-all>}}