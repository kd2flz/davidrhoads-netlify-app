+++
author = "David Rhoads"
date = 2021-05-07T10:30:00Z
description = "Spring is more than just a remission of Seasonal Depression"
image = "/uploads/washington-park-lake-sunset.jpg"
title = "The Music of Spring"

+++
There's something about spring. The sunset last night nearly took my breath away. Its brilliant reds and oranges lit up the western horizon over Washington Park Lake. Something about spring makes me happy and hopeful. A psychologist might say it's my annual bout of seasonal depression wearing off. A philosopher might add that spring is inherently the season of new life and hope for new beginnings.

Sitting at my desk, feeling the spring sun on my left shoulder, I am surrounded by the smell of freshly cut grass. Another hallmark of spring - the neighbors just mowed their lawn for the first time. Perhaps this afternoon I will find a spare hour to while away on our Ferris zero-turn.

This time of year makes me realize that summer is not far off, with its barbecues, hikes, and campfires. If spring is Mozart's Turkish March, summer is Beethoven's Ninth Symphony liberally giving out kisses to the whole world. Sure, summer has its sweaty "pass me the Gatorade" moments, but the long evenings at the lake or volleyball court more than make up for the heat.

So spring spirals upwards into summer, like Vaughn-Williams's Lark Ascending. I can only sit back, and enjoy the performance.

{{<comments the-music-of-spring>}}