+++
title = "About"
date = "2021-08-15"
aliases = ["about-me"]
[ author ]
  name = "David Rhoads"
+++

### Hello World
My name is David Rhoads. I am a student at Siena College in Albany, New York. I will use this website to share some of my thoughts on work, school, and the universe.

### Need A Professional Website?
One of my hobbies is web design. If you want a beautiful website like this one, [please reach out to me](mailto:kd2flz@outlook.com).

### Comments or Questions?
Feel free to [reach out](mailto:kd2flz@outlook.com) to me with questions and suggestions.
